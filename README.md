# ADS (Alerting and Detection) Geographic IP Anomoly Detection

Given a set of IPs a threshold, determine if a query IP is anomolous in
 terms of geographical distance.

# Obtaining the dataset.

With a full set of data this is a large archive. I have not included the uncompressed data in the gitlab repository. This means that building locally from gitlab is a bit of a pain.

To use the library, you must include the IP data set from IP2Location labeled IP2Location LITE IP-COUNTRY Database. It is available
[here](https://lite.ip2location.com/database/ip-country)

This database needs to be extracted, with the file `IP2LOCATION-LITE-DB5.CSV` put in
the `ads` directory of this repository (and overwrite what is there). From there, continue on to 'Setup'

# Setup

## Prequisites
* `Obtaining the dataset`
* Python3 - any `3.x` version should work - although this was written in `3.8.2`
* `python3-venv` from your package manger
* `make` available on the command line.

## Install

```
make
```

will create a virtual environment and install the package in editable mode in it.

## Development

from here you can run the tests

```
make test
```

linting

```
make lint
```

## Running the application

Use the following to run the application.

```
# Activate the virtual environment
. .venv/bin/activate
# Look at the help for the command line tool
(.venv) : anomolous-ip --help
# Usage of anomolous-ip is as follows:
(.venv) : anomolous-ip 1.0.0.1 1.0.0.2 1.0.0.3 1.0.3.254
# Output
1.0.3.254 anomolous: True
```

# Improvements

1. This tool is awfully slow - this is due to reading a 250mb dataset into memory every time the command line tool is called. In any production setting this dataset would sit in memory instead of being loaded in every time a query is needed.
1. The included dataset is miniscule, and merely a sample - it is wildly inaccurate for real usage. See `Obtaining the dataset.`
1. The default distance threshold was picked randomly. Real thought should be used to find a good item (or derive it with a large dataset of logs!). The threshold **could** be the maximum range from the mid point to the farthest IP.
1. This only supports IPv4. IPv6 would certainly be an improvement.
