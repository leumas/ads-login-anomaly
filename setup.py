from setuptools import setup, find_packages

setup(
    name='ads_login_anomaly',
    version='0.1.0',
    description='Determine anomonlies in login attempt locations.',
    author='Sam Christian',
    author_email='samchristian88@gmail.com',
    packages=find_packages(),
    python_requires='>=3',
    install_requires=[
        'docopt'
    ],
    extras_require={
        'dev': [
            'flake8',
            'pydocstyle',
            'mypy',
            'pytest',
            'pytest-cov',
        ]
    },
    project_urls={
        'Source': 'https://gitlab.com/leumas/'
    },
    include_package_data=True,
    scripts=['bin/anomolous-ip'],
)
