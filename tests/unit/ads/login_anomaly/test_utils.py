import unittest
from ads.login_anomaly.utils import find_center_point, distance_between


class TestFindCenterPoint(unittest.TestCase):
    """Tests for find_center_point."""

    def test_find_center_point_no_data(self):
        """When there is no data an assertion error is raised."""
        with self.assertRaises(AssertionError):
            find_center_point([])

    def test_find_center_simple_two_dimension(self):
        """In a simple two dimensional case the middle point is found."""
        coordinates = [(1, 10), (1, 20)]
        expected_middle = (1, 15)
        self.assertEqual(find_center_point(coordinates), expected_middle)

    def test_find_center_multiple_two_dimension(self):
        """With more than two coordinate pairs the midpoint is still found."""
        coordinates = [
            (1, 10),
            (2, 20),
            (3, 30),
            (4, 40),
            (5, 50)
        ]
        expected_middle = (3, 30)
        self.assertEqual(find_center_point(coordinates), expected_middle)

    def test_find_center_multiple_dimensions(self):
        """With more than two dimensions, the midpoint is still found."""
        coordinates = [
            (1, 10, 100, 1000),
            (2, 20, 200, 2000),
            (3, 30, 300, 3000),
            (4, 40, 400, 4000),
            (5, 50, 500, 5000),
        ]
        expected_midpoint = (3, 30, 300, 3000)
        self.assertEqual(find_center_point(coordinates), expected_midpoint)


class TestDistanceBetween(unittest.TestCase):
    """Tests for distance_between."""

    def test_distance_between_2d(self):
        """Test the distance between two points in two dimensions."""
        c1 = (1, 10)
        c2 = (1, 20)

        self.assertEqual(distance_between(c1, c2), 10)

    def test_distance_between_multi(self):
        """The distance between a multi dimensional item is still correct."""
        c1 = (0, 0, 0, 0, 10000)
        c2 = (0, 0, 0, 0, 0)
        self.assertEqual(distance_between(c1, c2), 10000)

    def test_different_lengths(self):
        """Two different lengthed coordinates raises an assertion error."""
        with self.assertRaises(AssertionError):
            distance_between((1, 2), (1, 2, 3))
