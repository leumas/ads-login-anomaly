import unittest

from ads.login_anomaly.ip_location import (
    CidrRange,
    ip_to_cidr_block,
    load_ip_data,
)
from tests.unit.ads.login_anomaly.utils import open_test_data


class TestLoadIpData(unittest.TestCase):

    def test_loading(self):
        """The test data should be loaded and resemble what is desired."""
        results = load_ip_data(open_test_data())
        self.assertEqual(len(results), 10)
        # Each item is length 2
        self.assertTrue(
            all([len(x) == 2 for x in results])
        )
        # The second item in the results is a CidrRange
        self.assertTrue(
            all([isinstance(x[1], CidrRange) for x in results])
        )

        # The results are sorted already (required for bisecting)
        self.assertTrue(sorted(results) == results)


class TestIpToCidrBlock(unittest.TestCase):

    def setUp(self):
        data_file = open_test_data()
        self.cidrs = load_ip_data(data_file)
        data_file.close()

    def test_far_left(self):
        """IPs on the far left of our test data will result in '-'."""
        test_ip = 16777214
        result = ip_to_cidr_block(test_ip, self.cidrs)
        self.assertEqual(result.city, '-')

    def test_far_right(self):
        """IPs to the far right of our data will result in no results."""
        test_ip = 16798208
        result = ip_to_cidr_block(test_ip, self.cidrs)
        self.assertIsNone(result)

    def test_middle_ground(self):
        """Middle ground IPs fall in between the start and end of the CIDR."""
        test_ip = 16781313
        result = ip_to_cidr_block(test_ip, self.cidrs)
        self.assertEqual(result.city, 'Tokyo')
        self.assertTrue(result.cidr_start <= test_ip <= result.cidr_end)
