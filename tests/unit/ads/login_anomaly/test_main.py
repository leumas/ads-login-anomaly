import unittest
from unittest import mock

from ads.login_anomaly.main import _is_anomalous, is_anomalous
from ads.login_anomaly.ip_location import load_ip_data
from tests.unit.ads.login_anomaly.utils import open_test_data


class Test_IsAnamolous(unittest.TestCase):

    def setUp(self):
        data_file = open_test_data()
        self.ip_data = load_ip_data(data_file)
        data_file.close()

    def test_basic_true(self):
        """In a simple case, we determine the proper result."""
        query_ip = 16779263
        other_ips = [16779264, 16779265, 16779266, 16779267]
        results = _is_anomalous(query_ip, other_ips, 10, self.ip_data)
        self.assertTrue(results)


class TestIsAnamolous(unittest.TestCase):
    def setUp(self):
        self.data_file = open_test_data()

    def tearDown(self):
        self.data_file.close()

    @mock.patch('ads.login_anomaly.main._is_anomalous')
    def test_basic(self, mock_internal):
        """In a simple case, we determine the proper result."""
        query_ip = "8.8.8.8"
        other_ips = ["1.1.1.1", "1.2.3.4"]
        is_anomalous(query_ip, other_ips, 10, self.data_file)
        # This test should be a little more fleshed out...
        # mock_internal.assert_called_once_with(...)
        mock_internal.assert_called_once()

    def test_full_false(self):
        """IPs in the same CIDR are considered not anomalous."""
        # These are all in Los Angeles from our test data...
        query_ip = "1.0.8.4"
        other_ips = ["1.0.8.1", "1.0.8.2", "1.0.8.3"]

        results = is_anomalous(query_ip, other_ips, 10, self.data_file)
        self.assertFalse(results)

    def test_full_true(self):
        """A query IP in a far distant land from the other IPs is anomalous."""
        # These are all in Los Angeles from our test data...
        other_ips = ["1.0.8.1", "1.0.8.2", "1.0.8.3"]
        # This is all the way in China, that should be considered an anomoly.
        query_ip = "1.0.3.254"

        results = is_anomalous(query_ip, other_ips, 10, self.data_file)
        self.assertFalse(results)
