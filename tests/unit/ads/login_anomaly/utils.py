"""Test Utilities."""


def open_test_data():
    """Open the test data."""
    return open('tests/unit/ads/login_anomaly/test_data', 'r')
