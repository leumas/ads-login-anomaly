.PHONY: ve
ve:
	python3 -m venv .venv
	.venv/bin/pip install -e .[dev]

.PHONY: test
test:
	.venv/bin/pytest --cov=ads tests

.PHONY: lint-mypy
lint-mypy:
	.venv/bin/mypy ads
	.venv/bin/mypy tests

.PHONY: lint-flake8
lint-flake8:
	.venv/bin/flake8 ads
	.venv/bin/flake8 tests

.PHONY: lint-pydocstyle
lint-pydocstyle:
	.venv/bin/pydocstyle ads
	.venv/bin/pydocstyle tests

.PHONY: lint
lint: lint-flake8 lint-pydocstyle

.PHONY: clean
clean:
	rm -rf .venv
	py3clean *

.PHONY: dist
dist: clean
	python3 setup.py sdist
