"""ads package."""

import os

_this_file = os.path.abspath(__file__)
_this_dir = os.path.dirname(_this_file)
data_file_location = os.path.join(
    _this_dir,
    'IP2LOCATION-LITE-DB5.CSV',
)
