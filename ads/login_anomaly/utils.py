"""Math utilities."""

from typing import List, Tuple
from math import sqrt


def find_center_point(
    coordinates: List[Tuple[float]]
) -> Tuple[float]:
    """Find the mid point of a set of coordinates."""
    amount_of_coordinates = len(coordinates)
    assert amount_of_coordinates != 0, "Must have at least one coordinate"
    number_of_dimensions = len(coordinates[0])

    sums = [0] * number_of_dimensions

    # The average of each dimension between all coordinates is the mid point.
    for coordinate in coordinates:
        for i in range(number_of_dimensions):
            sums[i] += coordinate[i]

    return tuple([sum_ / amount_of_coordinates for sum_ in sums])


def distance_between(
    coordinate_1: Tuple[float],
    coordinate_2: Tuple[float],
) -> float:
    """Find the distance between two coordinates."""
    # The points must have the same amount of dimensions.
    assert len(coordinate_1) == len(coordinate_2)

    # sqrt( (x2 -x1) ** 2 + (y2 - y1) ** 2 ... (n2 - n1) ** 2)
    interior = 0

    for i in range(len(coordinate_1)):
        interior += (coordinate_1[i] - coordinate_2[i]) ** 2

    return sqrt(interior)
