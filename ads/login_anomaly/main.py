"""Main functionality for login anamoly detection."""
import ipaddress

from typing import List, Tuple, TextIO

from ads.login_anomaly.ip_location import (
    CidrRange,
    load_ip_data,
    ip_to_cidr_block,
)
from ads.login_anomaly.utils import find_center_point, distance_between


def _is_anomalous(
    query_ip: int,
    other_ips: List[int],
    distance_threshold: int,
    ip_to_location_dataset: List[Tuple[int, CidrRange]]
) -> bool:
    """Determine if an integer IP is geographically anomolous from a set."""
    # Find the CIDRs of the given IPs
    cidrs = [ip_to_cidr_block(ip, ip_to_location_dataset) for ip in other_ips]

    # Find the coordinates (lat, long) of each CIDR
    ip_coordinates = [(cidr.latitude, cidr.longitude) for cidr in cidrs]
    mid_point = find_center_point(ip_coordinates)

    # Retrieve the latitude and longitutde (coordinate) of the query ip
    query_cidr_block = ip_to_cidr_block(query_ip, ip_to_location_dataset)
    query_point = (query_cidr_block.latitude, query_cidr_block.longitude)

    # The IP is anomolous if the distance threshold is lower than
    # the distance between the two points,
    # In other words,
    # If we find the mid point of a set of IPs, and draw a circle around it of
    # radius <distance_threshold>, does the point lie outside of that circle?
    return distance_threshold < distance_between(mid_point, query_point)


def is_anomalous(
    query_ip: str,
    other_ips: List[str],
    distance_threshold: int,
    data_file: TextIO,
) -> bool:
    """Determine if an IP is different from a set of other IPs.

    Given a threshold and historical logins, determine if an
    IP is gerographgically anomlous when compared to a set of IPs.
    """
    # Convert our stringed IPs to integers.
    query_ip = int(ipaddress.IPv4Address(query_ip))
    other_ips = [int(ipaddress.IPv4Address(ip)) for ip in other_ips]

    ip_location_dataset = load_ip_data(data_file)

    return _is_anomalous(
        query_ip,
        other_ips,
        distance_threshold,
        ip_location_dataset,
    )
