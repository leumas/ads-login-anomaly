"""Determine if a login is anomolous in its geographical location."""

import bisect
import csv

from dataclasses import dataclass
from typing import List, Optional, Tuple, TextIO


@dataclass(frozen=True)
class CidrRange:
    """Represent a CIDR range."""

    cidr_start: int
    cidr_end: int
    country_code: str
    country: str
    state: str
    city: str
    latitude: float
    longitude: float


def load_ip_data(
    file_handler: TextIO,
) -> List[Tuple[int, CidrRange]]:
    """Parse the Ip2Location CSV.

    The CSV format is assuemd to be sorted ascending.
    """
    csv_file = csv.reader(file_handler, delimiter=',', quotechar='"')
    cidr_ranges = []

    for line in csv_file:
        # Replace double quotes with nothing and split on commas.
        cidr_range = CidrRange(
            cidr_start=int(line[0]),
            cidr_end=int(line[1]),
            country_code=line[2],
            country=line[3],
            state=line[4],
            city=line[5],
            latitude=float(line[6]),
            longitude=float(line[7])
        )
        cidr_ranges.append((cidr_range.cidr_end, cidr_range))

    return cidr_ranges


def ip_to_cidr_block(
    ip: int,
    cidrs: List[Tuple[int, CidrRange]],
) -> Optional[CidrRange]:
    """Given a collection of CIDRs, Find the CIDR block that an IP belongs.

    If an IP is not captured at the end range, return None.
    """
    # Using bisect assumes there are zero gaps in the IP ranges.
    # Meaning that the entire IP space is represented in the dataset.
    # This seems to be the case - but may not always true, given a different
    # geolocation set.
    position = bisect.bisect_right(cidrs, (ip, ))
    try:
        # Extract the actual CIDR from the collection.
        result = cidrs[position][1]
    except IndexError:
        # If an index error occurs, the IP is to "the right" of the dataset
        # Meaning it is a "higher" IP than we have data on.
        result = None
    return result
